/*
               Tab Store
=== Copyright (C) 2019  Dakota Walsh ===

*/


/*
 * File Saving
 */

// cache tab saving dom
let file_saver     = document.getElementById("file-save");
let save_button    = document.getElementById("save-button");
let save_list      = document.getElementById("save-list");

let tabs_data      = {};
let tab_list = [];
let active_tabs_id_list = [];

// get the html for a checkbox item
function checkboxHtml(className, id, url, checked, text) {
	return (
		'<input ' +
			'type="checkbox" ' +
			'class="' + className +'" ' +
			'id="' + id + '" ' +
			'value="' + url + '" ' +
			(checked ? 'checked ' : '') +
			'> ' +
		'<label ' +
			'for="' + id + '">' +
			text +
		'</label>' +
		'<br>'
	);
}

// function for updating tabs
async function updateTabs() {
	// small delay to allow for currently closing tabs to close
	await new Promise((resolve, reject) => {
		setTimeout(() => { resolve(); }, 500);
	})
	tab_list = await browser.tabs.query({ currentWindow: true });

	// update the html list of tabs
	let checklist_HTML = "";
	active_tabs_id_list = [];
	for (let tab of tab_list) {
		active_tabs_id_list.push(tab.id);
		if (!tabs_data[tab.id]) { // if it's a new tab, then generate new data for it
			tabs_data[tab.id] = { url: tab.url, checked: true, title: tab.title, id: tab.id };
		} else { // if it's not then update its data
			tabs_data[tab.id].url = tab.url;
			tabs_data[tab.id].title = tab.title;
		}
		checklist_HTML += checkboxHtml(
			'save-checkbox',
			tab.id,
			tab.url,
			tabs_data[tab.id].checked,
			tab.title
		);
	}
	set_save_list(checklist_HTML);
}

// set the HTML save list to the current list of tabs, and bind events to update
function set_save_list(checklist_HTML) {
	save_list.innerHTML = checklist_HTML;
	checkbox_items = document.getElementsByClassName('save-checkbox');
	let keys = Object.keys(checkbox_items)
	for (let key in keys) {
		let item = checkbox_items[key];
		item.addEventListener("click", (event) => {
			tabs_data[item.id].checked = item.checked;
		});
	}
}

// subscribe to the tab events we care about
browser.tabs.onCreated .addListener(updateTabs);
browser.tabs.onRemoved .addListener(updateTabs);
browser.tabs.onUpdated .addListener(updateTabs);
browser.tabs.onAttached.addListener(updateTabs);
browser.tabs.onDetached.addListener(updateTabs);
browser.tabs.onMoved   .addListener(updateTabs);

// use our save button to run some code to ensure an updated tab list
save_button.addEventListener("click", (event) => {
	if (file_saver) {
		// make a list of urls based on the array of tabs
		let newline_seperated_tabs = '';
		let keys = Object.keys(tabs_data);
		for (let key of keys) {
			// check if the tab is selected, and still open
			if (tabs_data[key].checked && active_tabs_id_list.includes(parseInt(key))) {
				// tab.url requires the `tabs` permission
				newline_seperated_tabs += tabs_data[key].url + "\n";
			}
		}
		// encode the urls to be downloadable
		let data = "text/text;charset=utf-8," + encodeURIComponent(newline_seperated_tabs);
		// get the filename from the input box
		let filename = document.getElementById("file-name").value
		// update the file saver to include urls
		file_saver.setAttribute('href', 'data:' + data);
		file_saver.setAttribute('download', filename);
		// click the file saver
		file_saver.click();
	}
}, false);

updateTabs();

/*
 * File Loading
 */

// cache tab loading dom
let file_loader     = document.getElementById("file-load");
let load_button     = document.getElementById("load-button");
let load_list       = document.getElementById("load-list");
let open_all_button = document.getElementById("open-all-button");
let urls            = [];

function linkHtml(className, url) {
	return (
		'<span class="' + className + '">' +
			url +
		'</span>' +
		'<br>'
	);
}

// use our fancy load button to trigger the file selector
load_button.addEventListener("click", (event) => {
	if (file_loader) {
		file_loader.click();
	}
}, false);

open_all_button.addEventListener("click", (event) => {
	for (url of urls) {
		// ignore empty strings (if there is a new line at the end of the file it makes an empty string)
		if (url.length === 0) { continue; }
		// open the url
		browser.tabs.create({ url, active: false });
	}
});

// listen for newly selected files
file_loader.addEventListener("change", handleFiles, false);

function handleFiles(event) {
	// isolate the array of files
	let files = event.target.files;

	// read the files
	for (let file of files) {
		// create a file reader object
		let reader = new FileReader();
		// define onload callback
		reader.onload = (event) => {
			// turn the url list into an array
			urls = event.target.result.split('\n');
			let list_HTML = '';
			for (url of urls) {
				// ignore empty strings (if there is a new line at the end of the file it makes an empty string)
				if (url.length === 0) { continue; }
				// open the url
				list_HTML += linkHtml('load-list-item', url);
			}
			set_load_list(list_HTML);
		};
		reader.readAsText(file);
	}
}

// set the HTML load list to the current loaded tabs, and bind events to update
function set_load_list(list_HTML) {
	load_list.innerHTML = list_HTML;
	list_items = document.getElementsByClassName('load-list-item');
	open_all_button.style.display = "block";
	let keys = Object.keys(list_items)
	for (let key in keys) {
		let item = list_items[key]
		item.addEventListener("click", (event) => {
			browser.tabs.create({ url: item.innerText });
			// browser.tabs.create({ url: item.innerText, active: false });
		})
	}
}
