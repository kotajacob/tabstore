# TabStore

## Installation
The easiest way to install TabStore is by visiting [the firefox addon page here.](https://addons.mozilla.org/en-US/firefox/addon/tabstore/) If you would rather install the latest version from source you may clone this repository and use [web-ext.](https://www.npmjs.com/package/web-ext)

## About

This extension includes a browser sidebar that allows you to download
and save a list of your currently opened tabs. You can then restore one of these
plain text files and open all the tabs you had opened before. Or send the file to
friends or use the file for something like [wget](https://www.gnu.org/software/wget/) or [youtube-dl.](https://ytdl-org.github.io/youtube-dl/index.html)

## Usage

TabStore adds a new firefox "sidebar". To quickly open TabStore the hotkey Ctrl+Alt+T has been added.
Alternatively you can find it by opening the sidebar and selecting TabStore from the dropdown.
Once open you'll find a Save button, a Load button, and a text entry box.
To save all your tabs, type a name in the text box and click save. It will place the file in your downloads folder.
To load a previously saved tab file click load. It will open a file selection dialog and load your tabs upon selection.

## License

This code is licensed under the terms of the GPL3 license. 
See LICENSE in this repository for more details.

TabStore
Copyright (C) 2019  Dakota Walsh

